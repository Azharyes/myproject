package Week3;
public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        Shape shape1 = new Shape();
        Shape shape2 = new Shape(Color.Purple, true);
        System.out.println(shape1);
        System.out.println(shape2);
        shape1.setColor(Color.Black);
        shape1.setFilled(false);
        System.out.println(shape1.getColor() + "  " + shape1.isFilled());
        Rectangle shape3 = new Rectangle(5.0D, 2.0D);
        System.out.println(shape3);
        shape3.setFilled(true);
        System.out.println(shape1);
    }
}
