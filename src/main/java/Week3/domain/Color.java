package Week3.domain;

    public enum Color {
        Green,
        Blue,
        Red,
        Black,
        Purple;

        private Color() {
        }
    }

