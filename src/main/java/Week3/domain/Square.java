package Week3.domain;


public class Square extends Rectangle {
    public Square() {
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(Color color, boolean filled, double side) {
        super(color, filled, side, side);
    }

    public void setWidth(double side) {
        this.setSide(side);
    }

    public void setLength(double side) {
        this.setSide(side);
    }

    public void setSide(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    public double getSide() {
        return this.getLength();
    }

    public String toString() {
        return super.toString() + this.getSide() + "side";
    }
}
