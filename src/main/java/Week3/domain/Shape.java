package Week3.domain;

public class Shape {
    private Color color;
    private boolean filled;

    public Shape() {
        this.color = Color.Green;
        this.filled = true;
    }

    public Shape(Color color, boolean filled) {
        this.setColor(color);
        this.setFilled(filled);
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public boolean isFilled() {
        return this.filled;
    }

    public String toString() {
        return "color:" + this.color + "  and  " + (this.filled ? "  yes  " : "no  ");
    }
}


