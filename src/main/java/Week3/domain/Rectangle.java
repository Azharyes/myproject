package Week3.domain;

public class Rectangle extends Shape {
        private double Width;
        private double Length;

        public Rectangle() {
            this.setWidth(1.0D);
            this.setLength(1.0D);
        }

        public Rectangle(double width, double length) {
            this.setWidth(width);
            this.setLength(length);
        }

        public Rectangle(Color color, boolean filled, double width, double length) {
            super(color, filled);
            this.setLength(length);
            this.setWidth(width);
        }

        public double getLength() {
            return this.Length;
        }

        public double getWidth() {
            return this.Width;
        }

        public void setLength(double length) {
            this.Length = length;
        }

        public void setWidth(double width) {
            this.Width = width;
        }

        private double getArea() {
            double area = this.Length * this.Width;
            return area;
        }

        private double getPerimeter() {
            double perimeter = (this.Length + this.Width) * 2.0D;
            return perimeter;
        }

        public String toString() {
            return super.toString() + this.Width + " Width" + this.Length + " Length";
        }
    }


