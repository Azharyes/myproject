package Week3.domain;
public class Circle extends Shape {
    private double radius;

    public Circle() {
        this.radius = 1.0D;
    }

    public Circle(double radius) {
        this.setRadius(radius);
    }

    public double getRadius() {
        return this.radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    private double getArea() {
        double area = 3.141592653589793D * this.radius * this.radius;
        return area;
    }

    private double getPerimeter() {
        double perimeter = 6.283185307179586D * this.radius;
        return perimeter;
    }

    public String toString() {
        return super.toString() + "radius" + this.radius;
    }
}

